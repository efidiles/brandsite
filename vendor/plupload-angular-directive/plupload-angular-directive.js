'use strict';

angular.module('plupload.module', [])
  .directive('plUpload', ['$parse', '$rootScope',
    function($parse, $rootScope) {
      return {
        restrict: 'A',
        scope: {
          'plProgressModel': '=',
          'plFilesModel': '=',
          'plFiltersModel': '=',
          'plMultiParamsModel': '=',
          'plInstance': '='
        },
        link: function(scope, iElement, iAttrs) {

          scope.randomString = function(len, charSet) {
            charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var randomString = '';
            for (var i = 0; i < len; i++) {
              var randomPoz = Math.floor(Math.random() * charSet.length);
              randomString += charSet.substring(randomPoz, randomPoz + 1);
            }
            return randomString;
          }

          if (!iAttrs.id) {
            var randomValue = scope.randomString(5);
            iAttrs.$set('id', randomValue);
          }
          if (!iAttrs.plAutoUpload) {
            iAttrs.$set('plAutoUpload', 'true');
          }
          if (!iAttrs.plMaxFileSize) {
            iAttrs.$set('plMaxFileSize', '2048mb');
          }
          if (!iAttrs.plUrl) {
            iAttrs.$set('plUrl', 'upload.php');
          }
          if (!iAttrs.plFlashSwfUrl) {
            iAttrs.$set('plFlashSwfUrl', 'lib/plupload/plupload.flash.swf');
          }
          if (!iAttrs.plSilverlightXapUrl) {
            iAttrs.$set('plSilverlightXapUrl', 'lib/plupload/plupload.flash.silverlight.xap');
          }
          if (typeof scope.plFiltersModel == "undefined") {
            scope.filters = [
                {title : "Allowed files", extensions : "jpg,jpeg,gif,tiff,tif,png,psd,psb,zip,rar,tar,gzip,txt,doc,docx,xls,xlsx,ppt,pptx,pdf,rtf,mp3,ogg,wav,mov,mp4,f4v,flv,c4d,swf,eps,avi,ai,mpg,tga,ttf,JPG,JPEG,GIF,TIFF,TIF,PNG,PSD,PSB,ZIP,RAR,TAR,GZIP,TXT,DOC,DOCX,XLS,XLSX,PPT,PPTX,PDF,RTF,MP3,OGG,WAV,MOV,MP4,F4V,FLV,C4D,SWF,EPS,AVI,AI,MPG,TGA,TTF"},
            ];
          } else {
            scope.filters = scope.plFiltersModel;
          }

          var options = {
            runtimes: 'html5,flash,silverlight',
            browse_button: iAttrs.id,
            multi_selection: true,
            max_file_size: iAttrs.plMaxFileSize,
            url: iAttrs.plUrl,
            flash_swf_url: iAttrs.plFlashSwfUrl,
            silverlight_xap_url: iAttrs.plSilverlightXapUrl,
            filters: scope.filters,
            'urlstream_upload': true
          }

          if (scope.plMultiParamsModel) {
            options.multipart_params = scope.plMultiParamsModel;
          }

          var uploader = new plupload.Uploader(options);

          uploader.init();

          uploader.bind('Error', function(up, err) {
            if (iAttrs.onFileError) {
              scope.$parent.$apply(onFileError);
            }
            debugger;
            if(err.message === 'File extension error.') {
              $rootScope.lightbox.open({type: 'confirmation.error', message1: 'File type not allowed: ' + err.file.name});
            } else if(err.message === 'File size error.') {
              $rootScope.lightbox.open({type: 'confirmation.error', message1: 'File too large. Must be less than 2GB'});
            } else {
              $rootScope.lightbox.open({type: 'confirmation.error', message1: 'An error occured while uploading file: ' + err.file.name, message2: 'Try again and if the error persists please contact the brand team.'});
              //alert("Cannot upload, error: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
            }
            up.refresh(); // Reposition Flash/Silverlight
          });

          uploader.bind('FilesAdded', function(up, files) {

            //uploader.start();
            scope.$apply(function() {

              if (iAttrs.plFilesModel) {
                angular.forEach(files, function(file, key) {
                  scope.plFilesModel.push(file);
                });
              }

              if (iAttrs.onFileAdded) {
                scope.$parent.$eval(iAttrs.onFileAdded);
              }
            });
            if (iAttrs.plAutoUpload == "true") {
              uploader.start();
            }

          });

          uploader.bind('FileUploaded', function(up, file, res) {

            if (iAttrs.onFileUploaded) {
              if (iAttrs.plFilesModel) {
                scope.$apply(function() {
                  angular.forEach(scope.plFilesModel, function(file, key) {
                    scope.allUploaded = false;
                    if (file.percent == 100)
                      scope.allUploaded = true;
                  });
                  if (scope.allUploaded) {
                    var fn = $parse(iAttrs.onFileUploaded);
                    fn(scope.$parent, {
                      $response: res
                    });
                  }
                });

              } else {
                var fn = $parse(iAttrs.onFileUploaded);
                scope.$apply(function() {
                  fn(scope.$parent, {
                    $response: res
                  });
                });
              }
              //scope.$parent.$apply(iAttrs.onFileUploaded);
            }
          });

          uploader.bind('UploadProgress', function(up, file) {

            if (!iAttrs.plProgressModel) {
              return;
            }

            if (iAttrs.plFilesModel) {
              scope.$apply(function() {
                scope.sum = 0;
                angular.forEach(scope.plFilesModel, function(file, key) {
                  scope.sum = scope.sum + file.percent;
                });
                scope.plProgressModel = scope.sum / scope.plFilesModel.length;
              });
            } else {
              scope.$apply(function() {
                scope.plProgressModel = file.percent;
              });
            }

            if (iAttrs.onFileProgress) {
              scope.$parent.$eval(iAttrs.onFileProgress);
            }

          });

          if (iAttrs.plInstance) {
            scope.plInstance = uploader;
          }

        }
      };
    }
  ])

;