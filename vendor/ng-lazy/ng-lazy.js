/*global angular*/
(function() {
    'use strict';
    angular.module('rn-lazy', []).
    directive('rnLazyBackground', ['$document', '$parse',
        function($document, $parse) {
            return {
                restrict: 'A',
                template: ' <div class="spacer"></div> <div class="img"> </div>',
                scope: {
                  rnLazyBackground: '@rnLazyBackground'
                },
                replace: false,
                link: function(scope, iElement, iAttrs) {
                    iElement.addClass('wrap-lazy-img');

                    function setLoading(elm) {
                        if (loader) {
                            elm.html('');
                            elm.append(loader);
                            elm.css({
                                'background': null
                            });
                        }
                    }
                    var loader = null;
                    if (angular.isDefined(iAttrs.rnLazyLoader)) {
                        loader = angular.element($(iAttrs.rnLazyLoader)).clone();
                    }

                    scope.$watch('rnLazyBackground', function(newValue) {

                        setLoading(iElement);
                        var src = scope.rnLazyBackground;
                        //setTimeout(function(){
                        var img = $document[0].createElement('img');

                        img.onload = function() {
                            if (loader) {
                                loader.remove();
                            }
                            if (angular.isDefined(iAttrs.rnLazyLoadingClass)) {
                                iElement.removeClass(iAttrs.rnLazyLoadingClass);
                            }
                            if (angular.isDefined(iAttrs.rnLazyLoadedClass)) {
                                iElement.addClass(iAttrs.rnLazyLoadedClass);
                            }
                            //detect ie8
                            if ((document.documentMode || 100) < 9) {
                              iElement.find('img').remove();
                              iElement.append('<img src="'+this.src+'" />');
                              iElement.find('.img').css({
                                  'background-image': 'none'
                              });
                            } else {
                              iElement.find('.img').css({
                                  'background-image': 'url(' + this.src + ')'
                              });
                            }
                        };
                        img.onerror = function() {
                            //console.log('error');
                        };
                        img.src = src;
                        //}, 100);
                    });
                }
            };
        }
    ]);
})();